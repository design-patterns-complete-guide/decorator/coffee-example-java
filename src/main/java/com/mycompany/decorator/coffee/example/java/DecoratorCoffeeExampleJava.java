package com.mycompany.decorator.coffee.example.java;

// Step 1: Define the Coffee interface (Component)
interface Coffee {
    double cost();
}

// Step 2: Create a Concrete Component (BasicCoffee)
class BasicCoffee implements Coffee {
    @Override
    public double cost() {
        return 5.0; // Basic coffee costs $5.0
    }
}

// Step 3: Create an abstract Decorator class
abstract class CoffeeDecorator implements Coffee {
    protected Coffee coffee;

    public CoffeeDecorator(Coffee coffee) {
        this.coffee = coffee;
    }

    @Override
    public double cost() {
        return coffee.cost();
    }
}

// Step 4: Create Concrete Decorator classes
class MilkDecorator extends CoffeeDecorator {
    public MilkDecorator(Coffee coffee) {
        super(coffee);
    }

    @Override
    public double cost() {
        return super.cost() + 2.0; // Add the cost of milk ($2.0)
    }
}

class SugarDecorator extends CoffeeDecorator {
    public SugarDecorator(Coffee coffee) {
        super(coffee);
    }

    @Override
    public double cost() {
        return super.cost() + 1.0; // Add the cost of sugar ($1.0)
    }
}
public class DecoratorCoffeeExampleJava {

    public static void main(String[] args) {
        Coffee coffee = new BasicCoffee();
        Coffee coffeeWithMilk = new MilkDecorator(coffee);
        Coffee coffeeWithMilkAndSugar = new SugarDecorator(coffeeWithMilk);

        System.out.println("Cost of Basic Coffee: $" + coffee.cost());
        System.out.println("Cost of Coffee with Milk: $" + coffeeWithMilk.cost());
        System.out.println("Cost of Coffee with Milk and Sugar: $" + coffeeWithMilkAndSugar.cost());
    }
}
